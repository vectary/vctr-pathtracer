#pragma once

#include <luxcore/luxcore.h>
using namespace luxrays;
using namespace luxcore;

#ifdef _WIN32
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <windows.h>

std::string utf8_encode(const std::wstring &wstr);
std::wstring utf8_decode(const std::string &str);
#endif

void scene_parse(Scene *scene, std::string str);
std::pair<unsigned char *, unsigned int> read_file_content(std::string filepath);
std::string flt2str(float number);

void replaceAll(std::string& str, const std::string& from, const std::string& to);