#include "luxutil.h"
#include "../utilities/log.h"

void scene_parse(Scene *scene, std::string str)
{
    Properties props;
    props.SetFromString(str);
    try
    {
        scene->Parse(props);
    }
    catch (const std::exception &e)
    {
        log_error(e.what());
    }
}

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cerrno>
#include <cstring>

#include <fstream>

#ifdef _WIN32
// Convert a wide Unicode string to an UTF8 string
std::string utf8_encode(const std::wstring &wstr)
{
    if (wstr.empty())
        return std::string();
    int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
    std::string strTo(size_needed, 0);
    WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
    return strTo;
}

// Convert an UTF8 string to a wide Unicode String
std::wstring utf8_decode(const std::string &str)
{
    if (str.empty())
        return std::wstring();
    int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
    std::wstring wstrTo(size_needed, 0);
    MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
    return wstrTo;
}
#endif

std::pair<unsigned char *, unsigned int> read_file_content(std::string filepath)
{
    log("...reading: " + filepath);

    // int file = fopen(filepath.c_str(), O_RDWR , 0600);
#ifdef _WIN32
    int wchars_num = MultiByteToWideChar(CP_UTF8, 0, filepath.c_str(), -1, NULL, 0);
    wchar_t *wstr = new wchar_t[wchars_num];
    MultiByteToWideChar(CP_UTF8, 0, filepath.c_str(), -1, wstr, wchars_num);
#endif

#ifdef _WIN32
    struct _stat64i32 statbuf;
    _wstat(wstr, &statbuf);
    log("...size: " + std::to_string(statbuf.st_size));
    unsigned char *mem = (unsigned char *)malloc(statbuf.st_size);

    FILE *f = _wfopen(wstr, L"rb");
    delete[] wstr;
#else
    struct stat statbuf;
    stat(filepath.c_str(), &statbuf);
    log("...size: " + std::to_string(statbuf.st_size));
    unsigned char *mem = (unsigned char *)malloc(statbuf.st_size);

    FILE *f = fopen(filepath.c_str(), "rb");
#endif
    if (f)
    {
        fread(mem, 1, statbuf.st_size, f);
        fclose(f);
    }
    else
    {
        log_error("failed to open file" + std::string(strerror(errno)));
    }
    return std::make_pair(mem, statbuf.st_size);
}

#include <sstream>
#include <iomanip>
std::string flt2str(float number)
{
    std::stringstream ss;
    ss << std::fixed << std::setprecision(42) << number;
    return ss.str();
}

void replaceAll(std::string &str, const std::string &from, const std::string &to)
{
    if (from.empty())
        return;
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}