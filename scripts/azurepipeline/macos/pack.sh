#!/bin/bash
#
cd ./LuxCore
DEPS_SOURCE=`pwd`/macos


echo "PACK PT"
mkdir release_OSX_PT
cd release_OSX_PT
cp ../build/Release/pt ./

cp -f $DEPS_SOURCE/lib/libomp.dylib ./
cp -f $DEPS_SOURCE/lib/libembree3.3.dylib ./
cp -f $DEPS_SOURCE/lib/libOpenImageDenoise.1.2.1.dylib ./
cp -f $DEPS_SOURCE/lib/libtbb.dylib ./
cp -f $DEPS_SOURCE/lib/libtiff.5.dylib ./
cp -f $DEPS_SOURCE/lib/libOpenImageIO.1.8.dylib ./
cp -f $DEPS_SOURCE/lib/libtbbmalloc.dylib ./
cp -f $DEPS_SOURCE/lib/libcuda.dylib ./
cp -f $DEPS_SOURCE/lib/libnvrtc.dylib ./

install_name_tool -change @rpath/libomp.dylib @executable_path/./libomp.dylib ./pt
install_name_tool -change @rpath/libembree3.3.dylib @executable_path/./libembree3.3.dylib ./pt
install_name_tool -change @rpath/libtbb.dylib @executable_path/./libtbb.dylib ./pt
install_name_tool -change @rpath/libOpenImageIO.1.8.dylib @executable_path/./libOpenImageIO.1.8.dylib ./pt
install_name_tool -change @rpath/libtbbmalloc.dylib @executable_path/./libtbbmalloc.dylib ./pt
install_name_tool -change @rpath/libtiff.5.dylib @executable_path/./libtiff.5.dylib ./pt
install_name_tool -change @rpath/libOpenImageDenoise.0.dylib @executable_path/./libOpenImageDenoise.1.2.1.dylib ./pt

cd ..
tar -cvf pt.tar ./release_OSX_PT
mv pt.tar $BUILD_ARTIFACTSTAGINGDIRECTORY/pt-$VERSION_STRING-mac64.tar
echo "PT PACKED"
