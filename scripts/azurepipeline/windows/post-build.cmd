:: Gathering and packing binaries
echo %VERSION_STRING%
if "%VERSION_STRING%" EQU "" (
     set VERSION_STRING=latest
)
echo %VERSION_STRING%

:: cd WindowsCompile

:: if "%FINAL%" EQU "TRUE" (
::     call create-sdk.bat %1
::     set LUX_LATEST=luxcorerender-%VERSION_STRING%-win64-sdk
:: ) else (
::     call create-standalone.bat %1
    set LUX_LATEST=pt-%VERSION_STRING%-win64
:: )

:: move %DIR% %LUX_LATEST%
:: .\support\bin\7z.exe a %LUX_LATEST%.zip %LUX_LATEST%
:: copy %LUX_LATEST%.zip %BUILD_ARTIFACTSTAGINGDIRECTORY%

:: @echo ##vso[task.setvariable variable=version_string]%VERSION_STRING%

cd
dir

SET DIR=LuxCore
:: Remove folder if it already exists
rd /s /q %DIR%
md %DIR%

xcopy .\WindowsCompileDeps\x64\Release\lib\OpenImageDenoise.dll %DIR%
xcopy .\WindowsCompileDeps\x64\Release\lib\denoise.exe %DIR%
xcopy .\WindowsCompileDeps\x64\Release\lib\embree3.dll %DIR%
xcopy .\WindowsCompileDeps\x64\Release\lib\tbb.dll %DIR%
xcopy .\WindowsCompileDeps\x64\Release\lib\tbbmalloc.dll %DIR%
xcopy .\WindowsCompileDeps\x64\Release\lib\OpenImageIO.dll %DIR%
xcopy .\WindowsCompileDeps\x64\Release\lib\nvrtc64*.dll %DIR%
xcopy .\WindowsCompileDeps\x64\Release\lib\nvrtc-builtins*.dll %DIR%
xcopy .\WindowsCompile\Build_CMake\LuxCore\bin\Release\pt.exe %DIR%

move %DIR% %LUX_LATEST%
.\WindowsCompile\support\bin\7z.exe a %LUX_LATEST%.zip %LUX_LATEST%
copy %LUX_LATEST%.zip %BUILD_ARTIFACTSTAGINGDIRECTORY%
